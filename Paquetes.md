# Seleccion de paquetes a instalar con Kickstart
Usamos el comando "%packages" para comenzar la seleccion de paquetes i "%end" para indicar el final.  
A el comando %packages podemos añadirle las siguientes opciones:
* --nobase # No instalamos el grupo de paquetes Base, solo para instalaciones de sistemas muy pequeños
* --resolvedeps # Instala los paquetes listados y automáticamente resuelve las dependencias
* --ignoredeps # Ignora las dependencias e instala los paquetes listados
* --ignoremissing # Ignora los paquetes y grupos faltantes en vez de detener la instalación para preguntar

En medio de estos dos comandos podemos especificar grupos de paquetes o paquetes individuales.  
Los grupos de paquetes Base y Core se instalan por defecto.  
  
Para instalar un grupo de paquetes lo indicamos con el simbolo @.
<pre>@base-x</pre>
Este grupo de paquetes nos proporciona:
<pre>
    glx-utils
    mesa-dri-drivers
    plymouth-system-theme
    xorg-x11-drv-ati
    xorg-x11-drv-evdev
    xorg-x11-drv-fbdev
    xorg-x11-drv-geode
    xorg-x11-drv-intel
    xorg-x11-drv-mga
    xorg-x11-drv-modesetting
    xorg-x11-drv-nouveau
    xorg-x11-drv-omap
    xorg-x11-drv-openchrome
    xorg-x11-drv-qxl
    xorg-x11-drv-synaptics
    xorg-x11-drv-vesa
    xorg-x11-drv-vmmouse
    xorg-x11-drv-vmware
    xorg-x11-drv-wacom
    xorg-x11-server-Xorg
    xorg-x11-utils
    xorg-x11-xauth
    xorg-x11-xinit
</pre>

El simbolo - sirve para rechazar paquetes individuales y grupos de paquetes
<pre>- xorg-x11-drv-ati</pre>

Instalamos todo menos los paquetes conflictivos.
<pre>
@Everything
-@Conflicts
</pre>