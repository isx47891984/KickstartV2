# keyboard

Teclados disponibles:
> be-latin1, bg, br-abnt2, cf, cz-lat2, cz-us-qwertz, de,
de-latin1, de-latin1-nodeadkeys, dk, dk-latin1, dvorak, es, et,
fi, fi-latin1, fr, fr-latin0, fr-latin1, fr-pc, fr_CH, fr_CH-latin1,
gr, hu, hu101, is-latin1, it, it-ibm, it2, jp106, la-latin1, mk-utf,
no, no-latin1, pl, pt-latin1, ro_win, ru, ru-cp1251, ru-ms, ru1, ru2, 
ru_win, se-latin1, sg, sg-latin1, sk-qwerty, slovene, speakup, 
speakup-lt, sv-latin1, sg, sg-latin1, sk-querty, slovene, trq, ua, 
uk, us, us-acentos

Lista disponible en <pre>/usr/lib/python2.2/site-packages/rhpl/keyboard_models.py</pre> parte del paquete rhpl.
