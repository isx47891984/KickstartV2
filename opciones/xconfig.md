# xconfig

* --noprobe

	> No prueba el monitor.
* --card=

	> Usar la tarjeta especificada; el nombre de esta tarjeta debería ser de la lista de tarjetas en /usr/share/hwdata/Cards del paquete hwdata. La lista de tarjetas también se puede encontrar en la pantalla Configuración de X de Configurador de Kickstart. Si este argumento no se proporciona, el programa de instalación probará el bus PCI para la tarjeta. Puesto que AGP es parte del bus PCI, las tarjetas AGP serán detectadas si son soportadas. El orden de verificación está determinado por el orden de escaneo de PCI de la tarjeta madre. 
* --videoram=

	> Especifica la cantidad de RAM de vídeo que tiene la tarjeta de vídeo.
* --monitor=

	> Usar el monitor especificado: el nombre del monitor debería de provenir de la lista de monitores en /usr/share/hwdata/MonitorsDB del paquete hwdata. La lista de monitores también se puede encontrar en la pantalla Configuración de X del Configurador de Kickstart. Esto es ignorado si se proporciona * --hsync o * --vsync. Si no se proporciona información del monitor, el programa de instalación tratará de probarlo automáticamente. 
* --hsync=

	> Especifica la frecuencia de sincronización horizontal del monitor.
* --vsync=

	> Especifica la frecuencia de sincronización vertical del monitor.
* --defaultdesktop=

	> Especifica GNOME o KDE para el escritorio por defecto (asume que los ambientes de escritorio GNOME y/o KDE han sido instalados a través de %packages). 
* --startxonboot

	> Usar una ventana de conexión gráfica en el sistema instalado.
* --resolution=

	> Especifica la resolución por defecto para el sistema X Window en el sistema instalado. Los valores válidos son 640x480, 800x600, 1024x768, 1152x864, 1280x1024, 1400x1050, 1600x1200. Asegúrese de especificar una resolución que sea compatible con la tarjeta de vídeo y monitor. 
* --depth=

	> Especifica la profundidad del color por defecto para el sistema X Window en el sistema instalado. Los valores válidos son 8, 16, 24 y 32. Asegúrese de especificar una profundidad de color que sea compatible con la tarjeta de vídeo y con el monitor.