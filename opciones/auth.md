# auth / authconfig

* --enablemd5 (Usa encriptacion MD5 para las contraseñas de usuario)
* --useshadow o --enableshadow (Usar contraseñas shadow)
* --enableldap (Activa el soporte LDAP. Para usar esta opción, debe instalar el paquete nss_ldap.)
	+ --ldapserver= (Usamos esta opción para especificar el nombre del servidor LDAP a utilizar.) 
	+ --ldapbasedn= (Usamos esta opción para especificar el DN (distinguished name) en su árbol de directorio LDAP bajo el cual la información de usuario es almacenada.)
* --enableldapauth (Usar LDAP como un método de autenticación.)
* --enableldaptls (Use bloqueos TLS (Transport Layer Security). Esta opción permite a LDAP enviar nombres de usuario encriptados y contraseñas a un servidor LDAP antes de la autenticación.)
* --enablekrb5 (Usa Kerberos 5 para autenticación de usuarios. Si usamos esta opción, deberíamos tener el paquete pam_krb5 instalado.)
	+ --krb5realm= (El entorno Kerberos 5 al cual su estación pertenece.)
	+ --krb5kdc= (El KDC (o KDCs) que sirve peticiones para el entorno. Si tiene múltiples KDCs en su entorno, separe sus nombres con comas (,).)
	+--krb5adminserver= (El KDC en su entorno que también está ejecutanso kadmind. Este servidor maneja el cambio de contraseñas y otras peticiones administrativas. Este servidor debe ser ejecutado en el KDC principal si tiene más de un KDC.)

Mas opciones [aqui](http://web.mit.edu/rhel-doc/3/rhel-sag-es-3/s1-kickstart2-options.html).
