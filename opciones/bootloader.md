# bootloader

* --append=   

	>Especifica los parámetros del kernel. Para especificar múltiples parámetros, sepárelos con espacios
* --driveorder=   

	>Especifica cuál es la primera unidad en el orden de arranque del BIOS
* --location=   

	>Especifica dónde se escribirá el registro de arranque. Los valores válidos son los siguientes: mbr (valor por defecto), partition (instala el gestor de arranque en el primer sector de la partición que contiene el kernel), o none (no instala el gestor de arranque). 
* --password=   

	>Si se está usando GRUB, configura la contraseña de GRUB especificada con esta opción
* --md5pass=   

	>Si se está usando GRUB, similar a --password= excepto que la contraseña debería estar ya encriptada
* --upgrade   

	>Actualizar la configuración del gestor de arranque existente, preservando las viejas entradas. Esta opción está solamente disponible para actualizaciones.