# Origen de la instalacion

* nfs (Instala desde un árbol de instalación de Red Hat alojado en un servidor NFS)
	+ --server= (Servidor que aloja el directorio de instalacion(hostname o ip))
	+ --dir= (Directorio que contiene el arbol de instalacion)
* cdrom (Instala desde la primera unidad de CD-ROM del sistema)
* harddrive (Instala desde un árbol de instalación de Red Hat en un disco local, el cual puede ser bien sea vfat o ext2)
	+ --partition= (Particion desde la cual se instala, por ejemplo: sda2, hda4, sdb1)
	+ --dir= (Directorio que contiene el arbol de instalacion)
* url (Instala desde un árbol de instalación de Red Hat a traves de FTP o HTTP)
	+ --url <URL>
		- http://server/directorio
		- ftp://user:pass@server/directorio