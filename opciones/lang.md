# lang

* --addsupport= 

	> Añade ademas los idiomas especificados (Separar por comas)  

Codigos de lenguajes disponibles:  
> af_ZA, sq_AL, ar_DZ, ar_BH, ar_EG, ar_IN, ar_IQ, ar_JO, ar_KW, ar_LB, ar_LY, ar_MA, ar_OM, ar_QA, ar_SA, ar_SD, ar_SY, ar_TN, ar_AE, ar_YE, as_IN, ast_ES, eu_ES, be_BY, bn_BD, bn_IN, bs_BA, br_FR, bg_BG, ca_ES, zh_HK, zh_CN, zh_TW, kw_GB, hr_HR, cs_CZ, da_DK, nl_BE, nl_NL, en_AU, en_BW, en_CA, en_DK, en_GB, en_HK, en_IN, en_IE, en_NZ, en_PH, en_SG, en_ZA, en_US, en_ZW, et_EE, fo_FO, fi_FI, fr_BE, fr_CA, fr_FR, fr_LU, fr_CH, gl_ES, de_AT, de_BE, de_DE, nds_DE, de_LU, de_CH, el_GR, kl_GL, gu_IN, he_IL, hi_IN, hu_HU, is_IS, id_ID, ia_FR, ga_IE, it_IT, it_CH, ja_JP, kn_IN, ks_IN, ko_KR, ky_KG, lo_LA, lv_LV, lt_LT, mk_MK, mai_IN, ml_IN, ms_MY, mt_MT, gv_GB, mr_IN, se_NO, ne_NP, nb_NO, nn_NO, oc_FR, or_IN, fa_IR, pl_PL, pt_BR, pt_PT, pa_IN, ro_RO, ru_RU, ru_UA, sr_RS, sr_RS, si_LK, sk_SK, sl_SI, es_AR, es_BO, es_CL, es_CO, es_CR, es_DO, es_SV, es_EC, es_GT, es_HN, es_MX, es_NI, es_PA, es_PY, es_PE, es_PR, es_ES, es_US, es_UY, es_VE, sv_FI, sv_SE, tl_PH, ta_IN, te_IN, th_TH, tr_TR, uk_UA, ur_PK, uz_UZ, wa_BE@euro, cy_GB, xh_ZA, zu_ZA.

Lista disponible en <pre>/usr/share/system-config-language/locale-list</pre> parte del paquete rhpl.