# Instalación headless con KickStart y VNC
Con este proyecto trataré de explicar que es Kickstart de RedHat, que opciones podemos especificar según nuestro objetivo y finalmente mostrar que los ficheros hacen lo que deberian.  
## Que es Kickstart y porque deberia usarlo?
Kickstart es el sistema de automatizacion de instalaciones de RedHat, con el podemos automatizar en mayor o menor medida nuestras instalaciones,  
por ejemplo podemos tener una instalacion totalmente automatizada o podemos dejar que los usuarios particiones a su gusto el disco duro.  
Es recomendable usar kickstart si nos vamos a enfrentar a  la tarea de realizar multiples instalaciones, por ejemplo una granja de servidores o un laboratorio.
## Que opciones podemos especificar?
Un fichero de configuracion de Kickstart consta de 3 partes:
1. [Comandos de Kickstart](https://gitlab.com/isx47891984/KickstartV2/blob/master/Opciones.md)
2. [Seleccion de paquetes](https://gitlab.com/isx47891984/KickstartV2/blob/master/Paquetes.md)  
3. [Scripts de pre y post instalacion](https://gitlab.com/isx47891984/KickstartV2/blob/master/Scripts.md)

Documentacion de Kickstart en español e inglés.  
"[Documentación Opciones KS ES](http://web.mit.edu/rhel-doc/3/rhel-sag-es-3/ch-kickstart2.html)"  
"[Documentación Opciones KS EN](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Installation_Guide/ch-kickstart2.html)"